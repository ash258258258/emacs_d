
;Add at 2023/08/02, exercism about
(defun my-eval-and-run-all-tests-in-buffer ()
  "Delete all loaded tests from the runtime, evaluate the current buffer and run all loaded tests with ert."
  (interactive)
  (ert-delete-all-tests)
  (eval-buffer)
  (ert 't))

(global-linum-mode 1) ;; this mode is about line number
(setq linum-format "%4d \u2502 ")

;; Add at 2023/08/06, telega
(use-package telega
     :commands (telega)
     :init (setq telega-proxies
		 '((:server "localhost"
			    :port "2081"
			    :enable t
			    :type (:@type "proxyTypeHttp")))
		 telega-chat-show-avatars nil)
     (setq telega-chat-fill-column 65)
     (setq telega-emoji-use-images nil)
     ;;(setq telega-server-libs-prefix "...")
     :config
     (telega-notifications-mode t)
     (telega-mode-line-mode 1)
     (define-key telega-msg-button-map "k" nil)
     )

(provide 'init)
